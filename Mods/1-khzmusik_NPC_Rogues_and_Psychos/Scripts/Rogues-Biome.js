/**
 * @file Generates XML text for entitygroups.xml which adds human NPCs to entity groups used for
 *       biome spawning.
 *       After generating, you should substitute the actual biome spawn entity group name for the
 *       one that is automatically generated.
 */

/**
 * Total probability for all NPCs per gamestage entry.
 * Usually 1 unless spawning into animal groups.
 */
const TOTAL_PROBABILITY = 0.5;

/** True to divide by the count of weapon varieties, instead of normalizing each character to 1 */
const DO_COUNT = true;

const BASELINE_CHARACTER = '__BASELINE__';

const factions = buildFactions();
const weapons = buildAllWeapons();
const characters = buildCharacters();
const gs = buildGameStageStrings();

const tiers = gs.length;

buildNpcs(factions, weapons);

calculateAllFactions(factions);

printResults(factions);

function buildCharacters() {
    return [
        // This is a "baseline" character with all weapons and the default health and armor.
        // It is here so that packs which only contain powerful NPCs will be scored appropriately.
        // It will be removed after probabilities are calculated.
        {
            name: BASELINE_CHARACTER,
            health: 300,
            armor: 10,
            weapons: ['EmptyHand', 'Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueAgedKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueHeavyKhz',
            health: 400,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueLeather01Khz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'FemaleRogueLeather02Khz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueAgedKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueBlondeKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueCowboyKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        },
        {
            name: 'MaleRogueYoungKhz',
            health: 300,
            armor: 10,
            weapons: ['Axe', 'Bat', 'Club', 'Knife', 'Machete', 'Spear',
                'AK47', 'AShotgun', 'DPistol', 'HRifle', 'LBow', 'M60', 'PipeMG', 'PipePistol',
                'PipeRifle', 'PipeShotgun', 'Pistol', 'PShotgun', 'RocketL', 'SMG', 'SRifle',
                'TRifle', 'XBow']
        }
    ]
}

function buildFactions() {
    return {
        bandits: {
            All: [],
            Melee: [],
            Ranged: []
        }
    }
}

function buildGameStageStrings() {
    return [
        'GS01',
        'GS50',
        'GS100',
        'GS200',
        'GS400',
        'GS800',
        // Boss gamestage 800
        'GS800'
    ];
}

function buildNpcs(factions, weapons) {
    factions.bandits.All = weapons.flatMap(weapon => {
        return characters.map(c => {
            return {
                name: `${c.name}${weapon.name}`,
                health: c.health,
                armor: c.armor,
                weapon: weapon,
                probs: [],
                character: c
            };
        });
    }).sort(npcSorter);
}

function buildAllWeapons() {
    return [
        {
            name: 'EmptyHand',
            dmg: 9.1,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Axe',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Bat',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Club',
            dmg: 10,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Knife',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Machete',
            dmg: 20,
            burst: 1,
            isRanged: false
        },
        {
            name: 'Spear',
            dmg: 15,
            burst: 1,
            isRanged: false
        },
        {
            name: 'AK47',
            dmg: 13,
            burst: 5,
            isRanged: true
        },
        {
            name: 'AShotgun',
            dmg: 10,
            burst: 4,
            isRanged: true
        },
        {
            name: 'DPistol',
            dmg: 30,
            burst: 4,
            isRanged: true
        },
        {
            name: 'HRifle',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
        {
            name: 'LBow',
            dmg: 30,
            burst: 1,
            isRanged: true
        },
        {
            name: 'M60',
            dmg: 12,
            burst: 10,
            isRanged: true
        },
        {
            name: 'PipeMG',
            dmg: 6,
            burst: 5,
            isRanged: true
        },
        {
            name: 'PipePistol',
            dmg: 12,
            burst: 3,
            isRanged: true
        },
        {
            name: 'PipeRifle',
            dmg: 32,
            burst: 1,
            isRanged: true
        },
        {
            name: 'PipeShotgun',
            dmg: 10,
            burst: 1,
            isRanged: true
        },
        {
            name: 'Pistol',
            dmg: 12,
            burst: 7,
            isRanged: true
        },
        {
            name: 'PShotgun',
            dmg: 10,
            burst: 2,
            isRanged: true
        },
        {
            name: 'RocketL',
            // This is roughly the max damage before it totally throws off the curve
            dmg: 140,
            burst: 1,
            isRanged: true
        },
        {
            name: 'SMG',
            dmg: 10,
            burst: 10,
            isRanged: true
        },
        {
            name: 'SRifle',
            dmg: 35,
            burst: 3,
            isRanged: true
        },
        {
            name: 'TRifle',
            dmg: 13,
            burst: 10,
            isRanged: true
        },
        {
            name: 'XBow',
            dmg: 35,
            burst: 1,
            isRanged: true
        },
    ];
}

function calculateAllFactions(factions) {
    for (var factionName in factions) {
        if (factions.hasOwnProperty(factionName)) {
            weaponCategoryCalculateAll(factions[factionName]);
        }
    }
}

function calculateScores(npcs) {
    let maxScore = 0;
    let minScore = Number.MAX_VALUE;
    let total = 0;

    for (var i = 0; i < npcs.length; i++) {
        var weaponScore = calculateWeaponScore(npcs[i].weapon);

        npcs[i].score =
            (0.25 * npcs[i].health) +
            (2 * npcs[i].armor) +
            weaponScore;

        maxScore = Math.max(maxScore, npcs[i].score);
        minScore = Math.min(minScore, npcs[i].score);
        total += npcs[i].score;
    }

    return {
        maxScore,
        minScore,
        total
    };
}

function calculateProbabilities(npcs, scores) {
    const { maxScore, minScore } = scores;
    const range = maxScore - minScore;

    for (let t = 0; t < tiers; t++) {
        // normalized tier: 0..1
        let tN = t / (tiers - 1);
        // tier ratio: -1 to 1.2, min tier to max tier
        // We use 1.2 and not 1 to eliminate the characters with the lowest scores
        // from the highest gamestages
        let tR = 2.4 * tN - 1;

        for (let i = 0; i < npcs.length; i++) {
            // normalized score, 0..1
            let sN = (npcs[i].score - minScore) / range;
            // a line that rotates around (0.5, 0.5), slope determined by tN;
            // the probability is the y-value on that line when x=sN
            var prob = (tR * sN) - (0.5 * tR) + 0.5;
            // Make sure it's in the range of (0,1)
            prob = Math.max(prob, 0);
            prob = Math.min(prob, 1);
            npcs[i].probs[t] = prob;
        }
    }
}

function calculateWeaponScore(weapon) {
    // Use damage over time
    return weapon.dmg * weapon.burst;

    // Use DOT and give higher scores to ranged
    // return weapon.dmg * weapon.burst * (weapon.isRanged ? 2 : 1);
}

function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function normalizeCharacterProbabilities(npcs) {
    characters.forEach(c => {
        // Find the NPCs that are the same character with different weapon variations
        const charVariations = npcs.filter(npc => npc.character.name == c.name);

        // Each character variation has a probs array with one value per gamestage tier;
        // these are the values that may need to be normalized
        for (let tier = 0; tier < tiers; tier++) {
            let divisor = charVariations.length;
            if (!DO_COUNT) {
                divisor = charVariations.reduce(
                    (prev, curr) => prev + curr.probs[tier],
                    0);
            }

            if (divisor <= 1)
                continue;

            charVariations.forEach(cnpc => {
                cnpc.probs[tier] = cnpc.probs[tier] / divisor;
            });
        }
    });
}

function normalizeGamestages(npcs) {
    for (let tier = 0; tier < tiers; tier++) {
        const total = npcs.reduce(
            (prev, curr) => prev + curr.probs[tier],
            0);

        for (const npc of npcs) {
            npc.probs[tier] = TOTAL_PROBABILITY * npc.probs[tier] / total;
        }
    }
}

function npcSorter(a, b) {
    return a.name.localeCompare(b.name);
}

function printResults(factions) {
    console.log(`<configs>`);
    for (var faction in factions) {
        if (factions.hasOwnProperty(faction)) {
            let factionName = `${faction[0].toUpperCase()}${faction.slice(1)}`;
            printFactionXml(factions[faction], factionName);
        }
    }
    console.log(`\n</configs>`);
}

function printFactionXml(faction, factionName) {
    console.log(`\n    <!-- ${factionName} -->`);

    // Guard against empty entries
    if (faction.All.length < 1)
        return;

    // Don't print the highest tier, it's boss only
    for (let t = 0; t < tiers - 1; t++) {
        let tierTotal = 0;
        console.log(`    <append xpath="/entitygroups/entitygroup[@name='${factionName}-${gs[t]}']">`);
        faction.All.forEach(npc => {
            // This should accurately round to the third decimal place
            var prob = Math.round((npc.probs[t] + Number.EPSILON) * 1000) / 1000;
            if (prob >= 0.001) {
                console.log(`        <entity name="${npc.name}" prob="${prob}" />`);
                // For double-checking
                tierTotal += prob;
            }
        });
        console.log(`    </append>`);
        console.log(`    <!-- Tier total: ${tierTotal} -->`);
    }
}

function removeUnusedNpcVariations(npcs) {
    for (let i = 0; i < npcs.length; i++) {
        if (npcs[i].character.name !== BASELINE_CHARACTER &&
            npcs[i].character.weapons.includes(npcs[i].weapon.name))
            continue;
        npcs.splice(i, 1);
        i--;
    }
}

/**
 * Scales the probabilities of all NPC character variations to the range of [targetMin, max].
 * The target minimum should be a very low number (so we still scale), and the max shouldn't go
 * higher than the existing max (so if the original total didn't add up to 1, the scaled total
 * won't either).
 * 
 * @param {Object} npcs All NPCs
 */
function scaleProbabilities(npcs) {
    characters.forEach(c => {
        // Find the NPCs that are the same character with different weapon variations
        const charVariations = npcs.filter(npc => npc.character.name == c.name);

        // Each character variation has a probs array with one value per gamestage tier;
        // these are the values that may need to be scaled
        for (let i = 0; i < tiers; i++) {
            let total = charVariations.reduce(
                (prev, curr) => prev + curr.probs[i],
                0);
            // If the raw total doesn't add up to 1, then do nothing, not even scaling
            // if (total <= 1)
            //     continue;

            // Scale to range [targetMin, max]
            let min = Number.MAX_VALUE;
            let max = Number.MIN_VALUE;
            charVariations.forEach(v => {
                min = Math.min(min, v.probs[i]);
                max = Math.max(max, v.probs[i]);
            });

            const targetMin = min / (10 * total);

            charVariations.forEach(cnpc => {
                // If max is the same as min, use the original so we don't divide by zero
                var normalized = max == min ? cnpc.probs[i] : (cnpc.probs[i] - min) / (max - min);
                cnpc.probs[i] = normalized * (max - targetMin) + targetMin;
            });
        }
    });
}

/**
 * Function to use to calculate probabilities for All weapons.
 *
 * @param {object} faction
 */
function weaponCategoryCalculateAll(faction) {
    const scores = calculateScores(faction.All);
    calculateProbabilities(faction.All, scores);
    removeUnusedNpcVariations(faction.All);
    // Scale first
    scaleProbabilities(faction.All);

    normalizeCharacterProbabilities(faction.All);

    normalizeGamestages(faction.All);
}

/**
 * Function to use to calculate probabilities for All, Ranged, and Melee independently.
 * 
 * Benefits:
 * * "All" has a greater variation in overall difficulty between gamestages - less likely to
 *   encounter any kind of ranged NPC at GS1 than at GS800, and vice versa for melee.
 * * "Melee" groups have greater variations in weapon probabilities.
 * 
 * Drawbacks:
 * * "All" has uneven mix of melee and ranged depending upon gamestage.
 * * No consistency in weapon spawning between gamestage groups for all, ranged, and melee.
 *   A player can encounter an NPC with a machete at GS1 in "All" but not in "Melee".
 * 
 * @param {object} faction
 */
function weaponCategoryCalculateIndependently(faction) {
    for (var weaponCategory in faction) {
        if (faction.hasOwnProperty(weaponCategory)) {
            const scores = calculateScores(faction[weaponCategory]);
            calculateProbabilities(faction[weaponCategory], scores);
            removeUnusedNpcVariations(faction[weaponCategory]);
            scaleProbabilities(faction[weaponCategory]);
            normalizeCharacterProbabilities(faction[weaponCategory]);
        }
    }
}

/**
 * Function to use to calculate probabilities for Ranged and Melee independently,
 * then combine them into All.
 * 
 * Benefits:
 * * "All" has an even mix of ranged and melee at all gamestages.
 * * Consistency in weapon spawning between gamestage groups for all, ranged, and melee.
 * * "Melee" groups have greater variations in weapon probabilities.
 * 
 * Drawbacks:
 * * "All" has a far less variation in overall difficulty between gamestages - just as likely to
 *   encounter some kind of ranged NPC at GS1 as GS800, and the same for melee.
 *   Example: at GS800, equal probability to encounter an NPC with an axe, as an NPC with an M60.
 * 
 * @param {object} faction
 */
function weaponCategoryCalculateRangedMeleeCombineAll(faction) {
    const scoresMelee = calculateScores(faction.Melee);
    calculateProbabilities(faction.Melee, scoresMelee);
    removeUnusedNpcVariations(faction.Melee);
    scaleProbabilities(faction.Melee);

    const scoresRanged = calculateScores(faction.Ranged);
    calculateProbabilities(faction.Ranged, scoresRanged);
    removeUnusedNpcVariations(faction.Ranged);
    scaleProbabilities(faction.Ranged);

    faction.All = deepClone(faction.Melee)
        .concat(deepClone(faction.Ranged))
        .sort(npcSorter);

    for (var weaponCategory in faction) {
        if (faction.hasOwnProperty(weaponCategory)) {
            normalizeCharacterProbabilities(faction[weaponCategory]);
        }
    }
}
