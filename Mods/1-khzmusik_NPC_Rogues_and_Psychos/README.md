# Rogues and Psychos

Adds "Rogue" and "Psycho" human NPCs to the game.
Both rogues and psychos are part of the Bandits faction.

All characters were created by khzmusik, using assets that are either free for re-use or CC-BY.
See the Technical Details section for more information and asset credits.

## Rogues

The pack contains these "Rogue" characters:

---

### Female Rogue Aged
![Female Rogue Aged](./UIAtlases/UIAtlas/npcFemaleRogueAgedKhz.png)

---

### Female Rogue Heavy
![Female Rogue Heavy](./UIAtlases/UIAtlas/npcFemaleRogueHeavyKhz.png)

---

### Female Rogue Leather 1
![Female Rogue Leather 1](./UIAtlases/UIAtlas/npcFemaleRogueLeather01Khz.png)

---

### Female Rogue Leather 2
![Female Rogue Leather 2](./UIAtlases/UIAtlas/npcFemaleRogueLeather02Khz.png)

---

<!-- WAS: MaleRogueJoe -->
### Male Rogue Aged
![Male Rogue Aged](./UIAtlases/UIAtlas/npcMaleRogueAgedKhz.png)

---

### Male Rogue Blonde
![Male Rogue Blonde](./UIAtlases/UIAtlas/npcMaleRogueBlondeKhz.png)

---

### Male Rogue Cowboy
![Male Rogue Cowboy](./UIAtlases/UIAtlas/npcMaleRogueCowboyKhz.png)

---

<!-- WAS: MaleRogueGareth -->
### Male Rogue Young
![Male Rogue Young](./UIAtlases/UIAtlas/npcMaleRogueYoungKhz.png)

---

Every rogue can wield all supported NPC weapons.

## Psychos

The pack contains these "Psycho" characters:

### Female Psycho Skinny
![Female Psycho Skinny](./UIAtlases/UIAtlas/npcFemalePsychoSkinnyKhz.png)

---

### Female Psycho Stocky
![Female Psycho Stocky](./UIAtlases/UIAtlas/npcFemalePsychoStockyKhz.png)

---

### Male Psycho Skinny
![Male Psycho Skinny](./UIAtlases/UIAtlas/npcMalePsychoSkinnyKhz.png)

---

### Male Psycho Brute
![Male Psycho Brute](./UIAtlases/UIAtlas/npcMalePsychoBruteKhz.png)

---

_Most_ psychos wield all supported NPC weapons.

The exception is the **Male Psycho Brute.**
He does _not_ wield:

* Wooden bow
* Pistol
* Desert Eagle
* Knife
* Pipe Pistol
* Spear
* Hunting Rifle
* Pump Shotgun
* SMG
* Sniper Rifle

This is due to his size and posture,
which make him incompatible with the animations for those weapons.

## Dependent and Compatible Modlets

This modlet is dependent upon the `0-NPCCore` modlet,
and that modlet is in turn dependent upon the `0-SCore` modlet.

So far as I am aware, it should work with any versions of those modlets.

## Technical Details

This modlet includes new non-XML resources (Unity assets).
These resources are _not_ pushed from server to client.
For this reason, this modlet must be installed on both servers and clients.

While this modlet does not contain custom C# code, the modules it depends upon do.
You **must** run 7 Days To Die with **EAC off.**

### Spawning

Both Rogues and Psychos are spawned into biome spawns.

Rogues:
  * have a normal probability to spawn in the winter biome, and are tough (GS100 equivalent)
  * have a normal probability to spawn in non-forest cities, but are weaker (GS01 equivalent)
  * have a low probability to spawn in forest cities, and are weaker (GS01 equivalent)

Psychos:
  * have a low probability to spawn in the wasteland biome, and are tougher (GS200 equivalent)
  * have a normal probability to spawn in wasteland cities, and are tougher (GS200 equivalent)
  * have a low probability to spawn in desert, and are less tough (GS50 equivalent)

Rogues are also spawned into the NPC Core entity groups used by NPC sleeper volumes in POIs:

* `npcBandits*`
* `npcEnemy*`

Psychos are not spawned into NPC Core sleeper volume groups,
They are only spawned into biome spawns and the new Psycho-specific sleeper volume groups.

### New Sleeper Volume Groups

This modlet also comes with new Rogues and Psychos sleeper volume groups.
These groups will spawn _only_ Rogues, or _only_ Psychos, and not any other kind of bandit.

To use these groups:

1. Open the Prefab editor in 7D2D.
2. Choose the prefab into which you would like to spawn only Rogues or Psychos.
    (You can copy an existing prefab, or create an entirely new one if you prefer.)
3. For each sleeper volume in the prefab, select the prefab, and hit the `k` key.
4. In the group search bar, enter either "rogue" or "psycho" as appropriate.
    This will result in a few different sleeper volume groups dedicated to either rogues or psychos.
5. Click on the appropriate sleeper volume group.
    The "Group" value in the upper-right hand corner of the screen should change to the group you selected.
6. When done, save the prefab.

These are the available sleeper volume groups.

Rogues:
* Group NPC Rogues All
* Group NPC Rogues Melee
* Group NPC Rogues Ranged
* Group NPC Rogues Boss

Psychos:
* Group NPC Psychos All
* Group NPC Psychos Melee
* Group NPC Psychos Ranged
* Group NPC Psychos Boss

> :warning: If you make POIs with these sleeper volume groups,
> they **will throw exceptions** if this modlet is not installed.
> If you are distributing those POIs to other people,
> you should either distribute this modlet as well,
> or make it _very_ clear that they will need to install this modlet themselves.

### Basic and Advanced Versions

There are two versions of each set of characters:

* Basic characters, which do not have dialog options and can not be hired.
    They can also be spawned into horde groups (wandering or blood moon).
* Advanced characters, which have dialog interactions, and can be hired,
    assuming your standing with the Bandits faction is good enough.
    However, they can **not** be spawned into horde groups (wandering or blood moon) -
    if you try this, the game will throw an `InvalidCastException`.

By default, I am assuming most players will want the basic versions.
Those are the versions spawned into all entity groups, if you don't modify the XML.

The advanced versions are named the same as the basic versions, except with an "npc" prefix.
So, to spawn the advanced versions instead,
use XPath to insert the prefix before the names of all entities in the groups.

There is already XML in the `entitygroups.xml` file which will do all of this.
Simply un-comment the relevant parts of the XML.

### How the characters were created

The character models were created using Mixamo Fuse - a free program for making human-like characters.
Unfortunately, after Adobe bought it, it was discontinued and is no longer supported.
But it can still be used, and is still very good.

Once the models were created, I exported them to `.obj` files, and rigged them in
[Mixamo](https://www.mixamo.com).
From Mixamo, I exported them to Unity `.fbx` files.

I then imported them into Unity to set up ragdolls, rigging, tags, material shaders, etc.

None of this would be possible were it not for the help of Xyth and Darkstardragon - thank you!

If you would like to know how to do all this yourself,
Xyth has an excellent series of tutorials on YouTube:

https://www.youtube.com/c/DavidTaylorCIO/playlists

### Re-use in other mods/modlets

Any rights that I hold in these characters, I hereby place into the public domain (CC0).
Feel free to re-use them in any of your mods or modlets.

However, I don't hold the rights to all the assets in the models.
I have used assets from Mixamo Fuse, and do not hold any rights in those assets.
Those rights are retained by Fuse.

This sounds worse than it is.
Characters created in Mixamo Fuse may be used in any game (commerical or not) for free,
though you cannot repackage and sell the raw Fuse assets
(such as the body part models or individual clothing textures).
I do not provide those raw assets (and wouldn't know how), so this should not be a problem.

The characters also include assets provided by either NPC Core or The Fun Pimps.
These include, but are not limited to:

* Sounds and audio dialog
* Weapons and weapon models
* Animations

I can neither grant nor withhold permission to use these assets.

Because of this, use of these characters must abide by the requirements of **both** NPC Core
and The Fun Pimps.

If you are using them in your own mod or modlet for 7 Days To Die,
then this should not be a problem.
