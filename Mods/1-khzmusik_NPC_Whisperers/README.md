# NPC Whisperers

Adds "Whisperer" human NPCs to the game.
The Whisperers are inspired by the group of the same name from The Walking Dead.
(This modlet is not in any way associated with The Walking Dead.)

All characters were created by khzmusik, using assets that are either free for re-use or CC-BY.
See the Technical Details section for more information and asset credits.

> Whisperers are not attacked by zombies, and do not attack zombies themselves.
> In order to make this happen, zombies now use faction-based targeting and damage rules.
> This may have unexpected results if used with other mods/modlets that alter factions.

## Whisperers

The pack contains these characters:

### Whisperer Female
![Whisperer Female](./UIAtlases/UIAtlas/npcWhispererFemaleKhz.png)

### Whisperer Female 2
![Whisperer Female 2](./UIAtlases/UIAtlas/npcWhispererFemale2Khz.png)

### Whisperer Female 3
![Whisperer Female 3](./UIAtlases/UIAtlas/npcWhispererFemale3Khz.png)

### Whisperer Female Lumberjack
![Whisperer Female 2](./UIAtlases/UIAtlas/npcWhispererFemaleLumberjackKhz.png)

### Whisperer Female Boss
![Whisperer Female 2](./UIAtlases/UIAtlas/npcWhispererFemaleBossKhz.png)

### Whisperer Male
![Whisperer Male](./UIAtlases/UIAtlas/npcWhispererMaleKhz.png)

### Whisperer Male 2
![Whisperer Male 2](./UIAtlases/UIAtlas/npcWhispererMale2Khz.png)

### Whisperer Male 3
![Whisperer Male 3](./UIAtlases/UIAtlas/npcWhispererMale3Khz.png)

### Whisperer Male Robed
![Whisperer Male Robed](./UIAtlases/UIAtlas/npcWhispererMaleRobedKhz.png)

### Whisperer Male Lumberjack
![Whisperer Male 2](./UIAtlases/UIAtlas/npcWhispererMaleLumberjackKhz.png)

### Whisperer Male Young
![Whisperer Male Young](./UIAtlases/UIAtlas/npcWhispererMaleYoungKhz.png)

### Whisperer Male Boss
![Whisperer Male Boss](./UIAtlases/UIAtlas/npcWhispererMaleBossKhz.png)

Every character can wield all supported NPC weapons.

## Dependent and Compatible Modlets

This modlet is dependent upon the `0-NPCCore` modlet,
and that modlet is in turn dependent upon the `0-SCore` modlet.

So far as I am aware, it should work with any versions of those modlets.

## Technical Details

This modlet includes new non-XML resources (Unity assets).
These resources are _not_ pushed from server to client.
For this reason, this modlet should be installed on both servers and clients.

### Spawning

Whisperers usually don't wander alone, they wander in packs (with other whisperers, with zombies, or both).
So, I added them to the entity groups used for (non-blood-moon) hordes:

* `wanderingHordeStageGS*`
* `FwanderingHordeStageGS*`

When wandering with zombie hordes, Whisperers only use weapons that don't make loud sounds.
These include melee weapons, bows, and crossbows.

They are also spawned into the entity groups used by NPC sleeper volumes in POIs:

* `npcWhisperer*`
* `npcEnemy*`

The Whisperers spawned into these groups, may wield any weapon.

### Basic and Advanced Versions

There are two versions of each set of characters:

* Basic characters, which do not have dialog options and can not be hired.
    They can also be spawned into horde groups (wandering or blood moon).
* Advanced characters, which have dialog interactions, and can be hired,
    assuming your standing with the Whisperers faction is good enough.
    However, they can **not** be spawned into horde groups (wandering or blood moon) -
    if you try this, the game will throw an `InvalidCastException`.

By default, I am assuming most players will want the basic versions.
Those are the versions spawned into all entity groups, if you don't modify the XML.

The advanced versions are named the same as the basic versions, except with an "npc" prefix.
So, to spawn the advanced versions instead,
use XPath to insert the prefix before the names of all Whisperer entities in the groups.

But, remember that the advanced versions can't be spawned into wandering horde groups.
These are the groups that start with "wanderingHordeStageGS" or "FwanderingHordeStageGS".
If you want these NPCs to spawn outside of POIs,
they must spawn into the groups used for _biome_ spawning.

There is already XML in the `entitygroups.xml` file which will do all of this.
Simply un-comment the relevant parts of the XML.

### How the characters were created

The character models were created using Mixamo Fuse - a free program for making human-like characters.
Unfortunately, after Adobe bought it, it was discontinued and is no longer supported.
But it can still be used, and is still very good.

Once the models were created, I exported them to `.obj` files, and rigged them in
[Mixamo](https://www.mixamo.com).
From Mixamo, I exported them to Unity `.fbx` files.

I then imported them into Unity to set up ragdolls, rigging, tags, material shaders, etc.

None of this would be possible were it not for the help of Xyth and Darkstardragon - thank you!

If you would like to know how to do all this yourself,
Xyth has an excellent series of tutorials on YouTube:

https://www.youtube.com/c/DavidTaylorCIO/playlists

#### Whisperer Sounds

The sounds made by Whisperers were created by myself.

I recorded my own voice saying the lines,
then edited them in [Audacity](https://www.audacityteam.org/),
before importing them into Unity to package as Unity assets.

To get female sounds, I used XML to specify the sounds be played back at a higher pitch.
See `sounds.xml` for the values used.

### Re-use in other mods/modlets

Any rights that I hold in these characters, I hereby place into the public domain (CC0).
Feel free to re-use them in any of your mods or modlets.

However, I don't hold the rights to all the assets in the models.
I have used assets from Mixamo Fuse, and do not hold any rights in those assets.
Those rights are retained by Fuse.

This sounds worse than it is.
Characters created in Mixamo Fuse may be used in any game (commerical or not) for free,
though you cannot repackage and sell the assets.

I have also used Fuse assets provided by other community members.
See below for credits.

Obviously, the characters also use assets from The Fun Pimps (sounds, animations, etc).
The Fun Pimps retain the rights in those assets.

### Asset Credits

Some whisperers characters use the `M_DEAD FACE MASK` asset for Fuse.
This asset was provided by Mumphy:
https://community.7daystodie.com/profile/43889-mumpfy/

A big thank you to Mumphy for this asset.
